# Ara Repo Changelog

## 2024-02-13

- #50 Updated CLI scripts to work with multiple Ara instances

## 2024-01-23

- #51 Reworked repository README.md to be provide more information and be more user friendly

## 2024-01-15

- #8 Added multiple site Ara instances instructions in the compose README

## 2024-01-06

- #49 Added structure for multiple Ara instances on the same server reorganising the docker-compose files

## 2023-12-20

- #45 Fixed bug in backup script where test env rclone would not progress
- #48 Modularised encryption section into separate sub-scripts for easier maintenance

## 2023-12-19

- #46 Added check to backup script that checks if files exist before encrypt/transfer-only modes
- #47 Added encrypt_all option to backup script and updated env and READMEs
- #42 Fixed bug where backup script encryption would fail if other folders were in the backup directory

## 2023-12-18

- #44 Fixed bug where empty directories were not being deleted in cleanup of backup script

## 2023-12-17

- #43 Updated log / cli output for backup script

## 2023-12-12

- #41 Fixed Notice of Unencrypted Files in Backup Script

## 2023-12-03

- #17 Added Trouble Shooting Guide to and Updated README for backup script
- #39 Fixed bug where script would transfer unencrypted files to remote server

## 2023-11-30

- #38 Fixed bug where script would continue if encryption fails

## 2023-11-28

- #37 Added logging to the update script

## 2023-11-27

- #34 Added note about accessing WP-Content directory in README.md
- #35 Added logging to the backup script

## 2023-11-26

- #36 Fixed bug where backup script would run if no backup directory existed and updated gitignore to ignore backup files (.tar.gz, .sql)

## 2023-11-19

- #33 Added extra choice to backup script - tarball, extra testing options and help section

## 2023-11-14

- #32 Added "Active" choices for backup script - also validation checks for choices

## 2023-11-09

- #26 Reorganised update script to allow checks after variables are set
- #31 Fixed bug with Ansible mode in update script - hang if hash different - added docker-compose backup before overwrite
- #30 Fixed bug with update script - would fail if not run from within repository
- #29 Updated Docker Compose Images to WP 6 and MariaDB 11 - WP-CLI to 2.9.0

## 2023-11-08

- #28 Fixed bug with update script - user prompt was not validated
- #27 Fixed bug with .gitignore file ignoring directories but keeping .gitkeep files

## 2023-11-06

- #24 Added new modularised backup script to repository

## 2023-11-01

- #23 Fixed bug with update script not recreating docker containers on update

## 2023-10-30

- #21 Added Ansible Option to Update Ara Script

## 2023-10-29

- #19 Updated Docker Images used to WP 6.3 and MariaDB 11.1
- #20 Fixed incorrect ARA_HOME code in README.md
- #18 Added new `update.sh` script to update the Ara repo

## 2023-09-21

- #15 Updated `README.md` to Include More Detailed Setup Instructions and Troubleshooting

## 2023-09-20

- #5 Added Arguments to Backup Script

## 2023-09-19

- #16 Issue Closed as Out of Scope

## 2023-09-16

- #14 Addressed bug in `daily-backup.sh` script that resulted in already existing encrypted files to be encrypted again

## 2023-09-04

- #13 Updating Docker Compose MariaDB to 11.1.2 / WordPress 6.3.1

## 2023-08-28

- #7 Added Post About Using Cloudflare Reverse Proxy

## 2023-08-07

- #12 Rewrite of README.md to Include More Detailed Installation Instructions
- #10 Rework CLI Scripts to Include More Verbose Environmental Variables
- #9 Reworked Docker Based Environmental Variables to be More Verbose

## 2023-07-24

- #11 Bug Fix: Fixed issue with cli backup script after adding multi container support.

## 2023-07-23

- #6 Changed .env, docker-compose.yml and to improve multisite experience.

## 2023-07-18

- #4 Changed .env, docker-compose.yml and readme to Fix Errors on First Run

## 2023-07-11

- #3 Added Backup Script to Repository

## 2023-07-10

- #2 Enabled SSL and Environmental Variables in Apache

## 2023-07-07

- #1 Initial commit of project files
