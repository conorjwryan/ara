# DB Folder Information

---

Location: ara/docker/db

---

The DB folder contains two folders: `backups` and `files`

The `backups` folder is for storing database backups. The backups are created by the `data-backup.sh` script in the `files` folder which in turn is called by the `backup.sh` script in the `cli` in the root directory.
