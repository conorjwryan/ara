#!/bin/bash

mariadb-dump --all-databases -uroot -p"${MARIADB_ROOT_PASSWORD}" > /var/backups/$(date +%Y-%m-%d_%H-%M-%S)_${SITE_NAME}.sql