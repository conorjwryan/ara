# Compose Directory

This directory contains the docker-compose files for the Ara Project. Currently there are two docker compose files:

## Docker Compose Files

- [single-wp-single-db.yml](single-wp-single-db.yml) - This file will create one Ara instance with one WordPress site and one MySQL database (default).

If you would like to find out how to install one Ara instance on one machine follow the instructions on the README.md file in the [root of this repository](/README.md#installation-instructions-for-single-site).

- [multiple-wp-single-db.yml](multiple-wp-single-db.yml) - This file will create one Ara instance with multiple WordPress sites and one MySQL database.

## Installation Instructions for Multiple Sites

There are two ways to install multiple wordpress sites using Ara dependant on your needs.

1. Sharing the same database across multiple sites. This is recommended if you are deploying Ara on a memory constrained machine (Like a Digital Ocean Droplet with 1GB of RAM) for low traffic small business sites.

    Note: Do not use this method if you are deploying a high traffic WordPress / WooCommerce site. Use method 2 instead.

2. Using a separate database for each site. This is recommended if you are deploying Ara on a machine with 1.5GB of RAM or more.

In either case you still need to ensure you have the following prerequisites met from the [root of this repository](/README.md#prerequisites) such as having Docker/Compose installed, and having already generated your SSL certificates and keys.

### Method 1: Sharing the same database across multiple sites

1. Clone this repository to your machine.

    ```bash
    git clone https://gitlab.com/conorjwryan/ara.git
    ```

2. Move into the newly cloned directory.

    ```bash
    cd ara
    ```

3. Copy the example docker-compose file to `docker-compose.yml`. These docker compose files are located in the `docker/compose` directory.

    ```bash
    cp docker/compose/multiple-wp-single-db.yml docker-compose.yml
    ```

4. Copy the example .env file to `.env`.

    ```bash
    cp example.env .env
    ```

5. Open the `.env` file in your favourite text editor

    ```bash
    nano .env
    ```

    In this file we first fill in the variables the same as we would for a single site install. These are:

    ```bash
    SITE_NAME="red" # The short one word name of your site (e.g. example or site) no spaces - will be used as the folder name and the container name - keep it short
    SITE_URL="www.red.com" # The full URL of your site (e.g. www.example.com - without the https://)
    SITE_HOST_PORT="4431" # The port you want to expose the site on - default is 4430 but you can choose any port as long as this is reflected in the reverse proxy config

    MARIADB_ROOT_PASSWORD="123XYZ" # The root password for the MariaDB database
    MARIADB_DATABASE="wordpress" # The name of the database to be created  (e.g. wordpress)
    MARIADB_USER="wordpress_user" # The username for the WordPress database - like "worker"
    MARIADB_PASSWORD="ZXY321" # The password for the WordPress database user
    MARIADB_TABLE_PREFIX="red_" # The prefix for the WordPress database tables (for ease of use put SITE_NAME here) - DO NOT FORGET TO INCLUDE THE UNDERSCORE AT THE END
    ```

    Next go further down the file and make note of the following variables. They are automatically set for you but in order to add more sites you will need to make sure they are the same. These are:

    ```bash
    NET_NAME="${SITE_NAME}"
    NET_DB_IP=172.21.0.2
    NET_WP_IP=172.21.0.3
    NET_SUBNET=172.21.0.0/24
    ```

    Finally, go to the bottom of the file and add the following variables. These are the variables that will be used to create the second site. You can add as many sites as you want by following the same pattern.

    ```bash
    ## CHANGE THESE VARIABLES TO SUIT YOUR NEEDS ##

    SITE_NAME_2="blue"
    SITE_URL_2="www.blue.com"
    SITE_HOST_PORT_2="4432"
    MARIADB_TABLE_PREFIX_2="blue_"

    ## COPY INTO .ENV BUT DO NOT CHANGE THESE VARIABLES ##

    APACHE_CERT_2="/etc/ssl/${SITE_URL_2}.cert.pem"
    APACHE_KEY_2="/etc/ssl/${SITE_URL_2}.key.pem"
    APACHE_ROOT_2="html"

    CONTAINER_WP_2="${SITE_NAME_2}_wp"

    NET_WP_IP_2=172.21.0.4
    ```

    Save and exit the file.

6. Copy the `html` folder twice and to the value fo `SITE_NAME` and `SITE_NAME_2` you set in the `.env` file:

    Using the example above for site `red`:

    ``` bash
    cp -r docker/wordpress/sites/html docker/wordpress/sites/red
    ```

    Using the example above for site `blue`:

    ``` bash
    cp -r docker/wordpress/sites/html docker/wordpress/sites/blue
    ```

7. Change the ownership of the `sites` folder to the `www-data` user and group.

    ```bash
    sudo chown -R www-data:www-data docker/wordpress/sites
    ```

    This code will change the `html`, `red` and `blue` folders to be owned by the `www-data` user and group.

8. Add your SSL certificate and key to the `docker/apache/ssl` folder:

    ```bash
    sudo cp /path/to/your/cert.pem docker/apache/ssl/www.red.com.cert.pem
    sudo cp /path/to/your/key.pem docker/apache/ssl/www.red.com.key.pem
    ```

    ```bash
    sudo cp /path/to/your/cert.pem docker/apache/ssl/www.blue.com.cert.pem
    sudo cp /path/to/your/key.pem docker/apache/ssl/www.blue.com.key.pem
    ```

9. Change the ownership of the SSL certificate and key to `root`:

    ``` bash
    sudo chown root:root docker/apache/ssl/*
    ```

10. Start the containers:

    ``` bash
    docker compose up -d
    ```

If you have done this correctly you should see three containers running using the `docker ps` command:

```bash
docker ps
```

```bash
CONTAINER ID   IMAGES         COMMAND                  CREATED          STATUS          NAMES                                   
c3b2b0b5b2b2   mariadb        "docker-entrypoint.s…"   10 seconds ago   Up 9 seconds    red_db
d2b2b0b5b2b2   wordpress      "docker-entrypoint.s…"   10 seconds ago   Up 9 seconds    red_wp
e2b2b0b5b2b2   wordpress      "docker-entrypoint.s…"   10 seconds ago   Up 9 seconds    blue_wp                
```

### Method 2: Using a separate database for each site

This method is a lot easier to setup as you're just cloning the complete ara repository 2x using the single site instructions on the main page changing only a few vairables in the `.env` to stop network conflicts.

1. Clone this repository to your machine.

    ```bash
    git clone https://gitlab.com/conorjwryan/ara.git red
    ```

    ```bash
    git clone https://gitlab.com/conorjwryan/ara.git blue
    ```

2. In each of the cloned directories follow the instructions in the [root of this repository](/README.md#installation-instructions-for-single-site) to setup each site but crucially making sure the following variables are different across the two `.env` files like so:

    In the `red` directory:

    ```bash
    /home/conor/red/.env
    ```

    Fill in the variables the same as we would for a single site install. These are:

    ```bash
    SITE_NAME="red"
    SITE_URL="www.red.com"
    SITE_HOST_PORT="4431"
    ```

    etc. etc.

    and make a note of the `NET_` variables:

    ```bash
    NET_NAME="${SITE_NAME}"
    NET_DB_IP=172.21.1.2
    NET_WP_IP=172.21.1.3
    NET_SUBNET=172.21.1.0/24
    ```

    In the `blue` directory:

    ```bash
    /home/conor/blue/.env
    ```

    Fill in the variables the same as we would for a single site install. These are:

    ```bash
    SITE_NAME="blue"
    SITE_URL="www.blue.com"
    SITE_HOST_PORT="4432"
    ```

    etc. etc.

    and make sure that you change the network variables to be different to those in the `red` directory:

    ```bash
    NET_NAME="${SITE_NAME}"
    NET_DB_IP=172.21.2.2
    NET_WP_IP=172.21.2.3
    NET_SUBNET=172.21.2.0/24
    ```

    To ensure there are no conflicts with the `docker` network it is easiest to change the subnet completely as shown above:

    In that example red is using the `172.21.1.0/24` subnet and blue is using the `172.21.2.0/24` subnet.

3. Continue to follow the instructions in the [root of this repository](/README.md#installation-instructions-for-single-site) to setup each site.

## Known Issues & Troubleshooting

### Problem Running the `CLI` Scripts

As of now the `cli` scripts (both Update and Backup) are not working as they have not been optimised to work with multiple sites. This will be fixed in later updates.

To track the status of the fix see [Issue #50](https://gitlab.com/conorjwryan/ara/-/issues/50).

### Only One Site is Working in Single Database Mode

If you have followed the instructions above and only one site is working then you have probably forgotten to change the network variables in the `.env` file for the second site adding the prefix `_2` to each variable.

Also make sure you have changed the port number for the second site in the `.env` file and configured the reverse proxy to reflect this.

In the example above the red site is using port `4431` and the second site is using port `4432`.
