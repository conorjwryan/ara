#!/usr/bin/env bash

echo 'export APACHE_ROOT="'${APACHE_ROOT}'"' >> /etc/apache2/envvars
echo 'export SITE_URL="'${SITE_URL}'"' >> /etc/apache2/envvars
echo 'export APACHE_CERT="'${APACHE_CERT}'"' >> /etc/apache2/envvars
echo 'export APACHE_KEY="'${APACHE_KEY}'"' >> /etc/apache2/envvars
service apache2 restart
a2enmod ssl
a2ensite default-ssl
service apache2 restart
service apache2 stop