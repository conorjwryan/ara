# Docker Folder Information

---

Location: ara/docker

---

This folder is for files and scripts transferred into the docker containers.

## Compose

The `compose` folder contains the docker-compose files for the Ara Project which are responsible for creating the Ara instance.

## DB

The DB folder contains two folders: `backups` and `files`

The `backups` folder is for storing database backups. The backups are created by the `data-backup.sh` script in the `files` folder which in turn is called by the `backup.sh` script in the `cli` in the root directory.

## WordPress

The `WordPress` directory contains 2 folders: `sites` and `files`.

The `files` directory contains files that are transferred into the WordPress container upon creation.

The sites directory contains the WordPress sites. Each site is a folder with the name of the site. By default, the `html` folder is where the site is stored.
