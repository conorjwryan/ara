# WordPress Folder Information

---

Location: ara/docker/wordpress

---

The `WordPress` directory contains 2 folders: `sites` and `files`.

The `files` directory contains files that are transferred into the WordPress container upon creation.

The sites directory contains the WordPress sites. Each site is a folder with the name of the site. By default, the `html` folder is where the site is stored.
