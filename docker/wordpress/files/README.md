# WordPress Files Folder Information

---

Location: ara/docker/wordpress/files

---

Contained in this folder are the files that are transferred into the WordPress container upon creation. THese include:

1. uploads.ini - This is the php.ini file that is used to increase the upload size of the WordPress site.

2. wp-cli.phar - This is the wp-cli binary copied from the official wp-cli file. It is copied into the wordpress container upon creation and is this is used to run wp-cli commands from the command line.

3. wp-install.sh - This is the script that is used to install WordPress. It is copied into the wordpress container upon creation and is this is used to install a bare-bones WordPress site which is then used to run ai1wm to restore a site from a backup.
