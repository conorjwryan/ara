# HTML Folder Information

---

Location: ara/docker/wordpress/sites/html

---

This is the default directory where the first WordPress site is stored. For now there is no direct access to the base WordPress files only the main `wp-content` folder, `plugins`, `themes` and `uploads`. This is to prevent accidental changes to the core WordPress files.

## Expected Changes

As the scope of this project expands it is likely that other sites and folders will be added, where you would only need to copy this folder and rename it to create a new site.
