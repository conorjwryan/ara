# WordPress Sites Folder Information

---

Location: ara/docker/wordpress/sites

---

Contained within this folder is are the WordPress site folders. By default there is one folder called `html` which is the default WordPress site. This folder is copied into the WordPress container upon creation and is used to store the WordPress site files.

## Expected Changes

It is hoped in future that this project can be used to host multiple WordPress sites in a dockerised environment, the idea being is that you only have to copy the `html` folder and rename it to create a new site. For now this project only works with one site `html` which is hardcoded into the `docker-compose.yml` file.
