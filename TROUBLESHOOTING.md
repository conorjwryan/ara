# Troubleshooting Guide

Here will be a list of common issues and how to fix them. For now if you have an issue please open an issue on the [GitLab Repository](https://gitlab.com/conorjwryan/ara) or [my website](https://www.conorjwryan.com/) and I will try to help you as soon as possible.
