# Project Ara

Project Ara is a dockerised WordPress environment based on the official `WordPress` and `MariaDB` docker images. It is designed to be used in a quick and easy way to get a `WordPress` site up and running with minimal effort using a `Reverse Proxy` like `Cloudflare Tunnel`. It is designed to be cross-platform (AMD64/ARM) so can be used on both a Digital Ocean Droplet or a Raspberry Pi.

## Outcomes

Following the instructions below `Project Ara` will result in:

- A WordPress container that is easily set up a `HTTPS` `WordPress` site.
- A `MariaDB` database container
- `WP-CLI` installed and configured to work within the `WordPress` container
- Associated `CLI` scripts ran server-side to backup the site and update the docker images. For more information on these scripts see the [CLI Scripts Directory](/cli/README.md).

## Uses

`Project Ara` is designed to be used in a quick and easy way to get a `WordPress` site up and running with minimal effort using a `Reverse Proxy` like `Cloudflare Tunnel` mainly focusing on `low-traffic` `small business` and `personal` sites.

### Server Requirements

`Project Ara` is designed to be used on a `Linux` server with `Docker` and `Docker Compose` installed. It is designed to be cross-platform (AMD64/ARM) so can be used on both a Digital Ocean Droplet or a Raspberry Pi.

Minimum requirements for `Project Ara` are of small `low-traffic` sites are:

- `1GB` of `RAM`
- 1 `CPU` Core

### Default (recommended) Configuration

Out of the box, `Project Ara` can easily be configured to handle `low-traffic` `WordPress` site. This is the default configuration and is recommended for most users. 1 `Ara` deployment (`WP` Site + `DB`) per minimum spec server.

### Multi-Site Configuration

There are two ways to configure `Project Ara` to handle multiple sites. Either by running 2 `WP` containers with 1 `DB` container (mini-multi) or running 1 `WP` container with 1 `DB` container (true-multi). Instructions on how to do this can be found in the [Optional Steps](#optional-steps) section below.

For `high-traffic` sites and `WooCommerce` stores, it is recommended to use a site with  `Digital Ocean` $12 `Droplet` or hardware with `1GB< RAM`.

## Before You Begin

1. The instructions below already assumes you have Docker and Docker Compose installed on your system.

    If you don't, please follow the instructions for your operating system here: <https://docs.docker.com/install/>.

2. This script also assumes you have a reverse proxy set up with a domain name.

    [See here](https://www.conorjwryan.com/posts/cloudflare-tunnel-reverse-proxy/) for a tutorial on how to set up a reverse proxy using Cloudflare.

3. You will need to have a valid SSL certificate and key for your domain name.

    These should be placed in the `docker/apache/ssl` folder under the names `www.example.com.cert.pem` and `www.example.com.key.pem` respectively.

4. Ara assumes that the user installs the repository in the home directory of their user `~/ara`.

    If Ara is installed in a different directory (like: `/var/www/ara`) or renames the ara directory to something else the project will still work however the CLI scripts will not work. See the [Optional Steps](#optional-steps) section for more information.

## Installation Instructions for Single Site

A written tutorial can be found on my site [here](https://www.conorjwryan.com/posts/ara-single-setup/) which contains expanded steps and associated images and outputs.

### Quick Start

1. Clone the repository and change into the directory:

    ``` bash
    git clone https://gitlab.com/conorjwryan/ara.git
    ```

2. Move into the newly cloned directory:

    ``` bash
    cd ara
    ```

3. Copy the example docker-compose file to `docker-compose.yml`:

    ``` bash
    cp docker/compose/single-wp-single-db.yml docker-compose.yml
    ```

    You should not need to edit this file as it is set up to work with the `.env` file.

4. Copy the example .env file to `.env`:

    ``` bash
    cp example.env .env
    ```

5. Edit the `.env` file to suit your needs:

    ``` bash
    nano .env
    ```

    Follow the instructions in the file to set the variables.

    Key variables to set are:

    ``` bash
    SITE_NAME # The short one word name of your site (e.g. example or site) no spaces - will be used as the folder name and the container name - keep it short
    SITE_URL # The full URL of your site (e.g. www.example.com - without the https://)
    SITE_HOST_PORT="4430" # The port you want to expose the site on
    MARIADB_ROOT_PASSWORD # The root password for the MariaDB database
    MARIADB_DATABASE # The name of the database to be created  (e.g. wordpress)
    MARIADB_USER # The username for the WordPress database - like "worker"
    MARIADB_PASSWORD # The password for the WordPress database user
    Optional: MARIADB_TABLE_PREFIX # The prefix for the WordPress database tables (can be left as default "wp_")
    ```

6. Copy the `html` folder to the name `SITE_NAME` you set in the `.env` file:

    ``` bash
    cp -r docker/wordpress/sites/html docker/wordpress/sites/SITE_NAME
    ```

7. Change the ownership of the `sites` folder to `www-data`:

    ``` bash
    sudo chown -R www-data:www-data docker/wordpress/sites
    ```

8. Add your SSL certificate and key to the `docker/apache/ssl` folder:

    ``` bash
    cp www.your-site.com.cert.pem docker/apache/ssl/www.your-site.com.cert.pem
    ```

    ```bash
    cp www.your-site.com.key.pem docker/apache/ssl/www.your-site.com.key.pem
    ```

9. Change the ownership of the SSL certificate and key to `root`:

    ``` bash
    sudo chown root:root docker/apache/ssl/*
    ```

10. Start the containers:

    ``` bash
    docker compose up -d
    ```

11. Check the status of the containers:

    ``` bash
    docker compose ps
    ```

    You should see something like this:

    ``` bash
    Name                   Command               State                 Ports
    --------------------------------------------------------------------------------------------
    web_db                                        Up
    web_wp                                        Up

    ```

    As long as the state of the containers is `Up` you should be good to go.

12. Make sure you have setup your reverse proxy to point to the correct port (the one you set in the `.env` file).

13. Navigate to your site in your browser and you should see the WordPress setup page.

## Optional Steps

### Installing Ara in a Non-Standard Location

For those that have installed their ARA instance in a non-standard location (like `/var/www/ara`), or under a non-standard name (like `example-website`), you need to add this line to your .bashrc file in order for `CLI` related scripts to work:

```bash
export ARA_HOME="/var/www/ara/"

# or

export ARA_HOME="/var/www/example-website/"
```

You can do this by running the following command:

```bash
echo 'export ARA_HOME="/var/www/ara/"' >> ~/.bashrc 
```

Keep in mind: You will need to restart your terminal for this to take effect.

```bash
source ~/.bashrc
```

### Add Server-side Cronjob for Background WordPress Tasks

In order for WordPress to run background tasks (like checking for updates) you need to add a cronjob to your server. This can be done by running the following command:

```bash
crontab -e
```

Then add the following line to the bottom of the file:

```bash
*/5 * * * * docker exec -it <SITE_NAME>_wp wp cron event run --due-now --path=/var/www/html --allow-root
```

`SITE_NAME` is the name of the site you set in the `.env` file.

### Multiple Sites on the Same Server

If you would like to run multiple sites on the same server, you can do so by following the steps in the [`README.md`](docker/compose/README.md) file in the `docker/containers/` directory.

## Known Issues and Limitations

### Limited Visibility into WP-Contents

Ara is designed to be as simple as possible to use. This means that the user does not have access to the `wp-content` folder except to access to 3 folders: `plugins`, `themes`, and `uploads`.

Some plugins (backup and cache) produce extra directories in the `wp-content` folder. In order to see these folders, the user must run the following command:

```bash
docker compose exec -it <SITE_NAME>_wp ls -la /var/www/html/wp-content
```

`SITE_NAME` is the name of the shortened name of the site you set in the `.env` file. This command can be run from the ARA root directory and will show if there are any extra folders in the `wp-content` directory.

## Troubleshooting

Go here to see the [Troubleshooting Guide](TROUBLESHOOTING.md) of common problems and how to fix them.

## Other Projects

Thank you for taking the time to read through this whole document! If you like what you see why not [checkout my other projects](https://www.conorjwryan.com/projects/) including:

- `Ansible Playbooks`
- `Bash Scripts`
- Cloudflare-related tutorials
