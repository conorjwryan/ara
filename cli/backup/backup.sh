#!/bin/bash
# shellcheck source=/dev/null
# shellcheck disable=SC2154,SC2317
# Main Script - Ara Backup Bash Script
# Location: cli/backup/backup.sh
# Version 1.4.2
# Author: Conor Ryan
# Date: 2023-12-20
# Description: This script is used to backup the WordPress sites and Databases the Ara directory
####

cd "$(dirname "$0")" || exit 1

echo -e "Starting Backup Script";

####
# Add Functions
####

source ./functions/backup-functions-logic.sh

####
# Argument Validation
####
# Checks if the arguments passed to the script are valid
####

source ./pre/argument-validation-logic.sh

####
# Include Ara Home Logic and .env file check
####

log "Starting Script Pre-Checks";
echo "";

source ./pre/ara-home-logic.sh

####
# Setting Variables
####

log "Setting Variables";

set -o allexport
source "${ARA_HOME}".env 
set +o allexport

log "Variables Set";
echo "";

####
# Checking Prerequisites
####

source ./pre/script-prerequisite-logic.sh

log "End of Script Pre-Checks";
echo "";

####
# Backup Section
####


if [[ ${backup_enabled} == 1 ]]; then

    source ./copy/backup-section-logic.sh

else
    log "INFO: Backup Disabled";
    echo "";

fi

####
# Encryption Section
####

if [[ ${encrypt_enabled} == 1 ]]; then
    source ./encryption/encryption-section-logic.sh

else
	log "INFO: Encryption Disabled";
    echo "";

fi

####
# Transfer Section
####

if [[ ${transfer_enabled} == 1 ]]; then
    source ./transfer/transfer-section-logic.sh;

else
    log "INFO: Transfer Disabled";
    echo "";

fi

####
# Cleanup Section
####
# The cleanup section is the final section of the script responsible for the aftermath of the backup
####

source ./post/cleanup-section-logic.sh

log "End of Script"

####
# End of Script
###
exit 0;