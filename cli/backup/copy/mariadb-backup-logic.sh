#!/bin/bash
# shellcheck source=/dev/null
# shellcheck disable=SC2154
# MariaDB Backup Logic - Ara Backup Bash Script
# Location: cli/backup/copy/mariadb-section-logic.sh
# Version 1.1.2
# Author: Conor Ryan
# Date: 2023-12-17
# Description: This script involves the MariaDB container - backup and copy of associated database files
####

if [[ ${backup_enabled} != 1 ]]; then
    log "Backup is not enableded.";
    log "Illegal State: Backup Section should not be running."
    log "Aborting Script";
    exit 1;

fi

####
# MariaDB Database Section
####
# This section will dump the MariaDB database and move the dump to the host system
# Currently this is done via a script which is run on the MariaDB container
####

log "Performing Database Dump";

docker exec -i "${SITE_NAME}_db" ./var/data-backup.sh;

####
# MariaDB Database Output Transfer
####

log "Moving .sql Output";

find "${ARA_HOME}docker/db/backups/" -name '*.sql' -exec mv {} "${BACKUP_DIR}${date}/" \;

log "Finished Database Dump";
echo "";
