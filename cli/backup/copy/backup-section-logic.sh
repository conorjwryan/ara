#!/bin/bash
# shellcheck source=/dev/null
# shellcheck disable=SC2154
# Backup Section Logic - Ara Backup Bash Script
# Location: cli/backup/copy/backup-section-logic.sh
# Version 1.3.0
# Author: Conor Ryan
# Date: 2024-02-13
# Description: This script is the basis of the copy/backup section of the Ara Backup Bash Script
####

if [[ ${backup_enabled} != 1 ]]; then
    log "Backup is not enableded.";
    log "Illegal State: Backup Section should not be running."
    log "Aborting Script";
    exit 1;

fi

log "Start of Backup/Copy Process";
echo "";

####
# AI1WM Backup Section
####
# This section will backup the AI1WM plugin and move the backup to the host system
####

if [[ "${AI1WM_ACTIVE}" == 1 || "${AI1WM_ACTIVE_2}" == 1 ]]; then
    source ./copy/ai1wm-backup-logic.sh

else 
    log "INFO: AI1WM is not active, skipping..."
    echo "";

fi

####
# MariaDB Backup Section
####
# This section will backup the MariaDB database and move the backup to the host system
####

source ./copy/mariadb-backup-logic.sh

####
# Tarball Backup Section
####

source ./copy/tar-backup-logic.sh

####
# End Copy Section
####