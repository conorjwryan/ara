#!/bin/bash
# shellcheck source=/dev/null
# shellcheck disable=SC2154,SC2153
# Tar Backup Logic - Ara Backup Bash Script
# Location: cli/backup/copy/tar-backup-logic.sh
# Version 3.1.1
# Author: Conor Ryan
# Date: 2024-02-13
# Description: This script involves the the creation of the tarball based on script mode and moving it to the host system
####

if [[ ${backup_enabled} != 1 ]]; then
    log "ERROR: Backup is not enableded.";
    log "Illegal State: Backup Section should not be running."
    log "Aborting Script";
    exit 1;

fi

if [[ -z ${backup_mode} ]]; then
    log "ERROR: No backup mode set";
    log "Illegal State: Backup Backup Mode should be defined."
    log "Aborting Script";
    exit 1;

fi

echo "Archive Creation Section (tarball)";

####
# TAR Section
####
# This section will TAR the WordPress site and move the TAR to the host system
####

if [[ ${backup_mode} == "complete" || ${backup_mode} == "all" ]]; then
    log "Creating archive of everything in '${SITE_NAME}' (complete)";
    echo "";

    docker exec -i "${SITE_NAME}_wp" tar -czpf "/var/backups/${datetime}_${SITE_NAME}_complete.tgz"  "/var/www/${APACHE_ROOT}/";

    echo "";

    if [[ -n ${SITE_NAME_2} ]]; then
        log "Creating archive of everything in '${SITE_NAME_2}' (complete)";
        echo "";

        docker exec -i "${SITE_NAME_2}_wp" tar -czpf "/var/backups/${datetime}_${SITE_NAME_2}_complete.tgz"  "/var/www/${APACHE_ROOT_2}/";

        echo "";
    fi

fi

if [[ ${backup_mode} == "no-uploads" || ${backup_mode} == "all" ]]; then
    log "Creating archive of '$ARA_HOME'";
    log "Includes core files and selective backup of WP-Content"
    log "Only plugins, themes - no uploads or extra directories";
    echo "";

    log "Starting Backup of '${SITE_NAME}'";
    backup_site "plugins" "themes" "${SITE_NAME}" "${APACHE_ROOT}" "no-uploads"

    if [[ -n ${SITE_NAME_2} ]]; then
        log "Starting Backup of '${SITE_NAME_2}'";
        backup_site "plugins" "themes" "${SITE_NAME_2}" "${APACHE_ROOT_2}" "no-uploads"

    fi

fi

if [[ ${backup_mode} == "core" || ${backup_mode} == "all" ]]; then
    log "Creating archive of '${ARA_HOME}'";
    log "Includes core files and selective backup of WP-Content";
    log "Only plugins, themes and uploads - no extra directories";
    echo "";

    log "Starting Backup of '${SITE_NAME}'";
    backup_site "plugins" "themes" "uploads" "${SITE_NAME}" "${APACHE_ROOT}" "core"

    if [[ -n ${SITE_NAME_2} ]]; then
        log "Starting Backup of '${SITE_NAME_2}'";
        backup_site "plugins" "themes" "uploads" "${SITE_NAME_2}" "${APACHE_ROOT_2}" "core"

    fi

fi


####
# TAR Output Transfer
####

log "Moving Archive";

find "${ARA_HOME}docker/wordpress/backups/" -name '*.tgz' -exec mv {} "${BACKUP_DIR}${date}/" \;

log "Finished Archive Creation Section";
echo "";