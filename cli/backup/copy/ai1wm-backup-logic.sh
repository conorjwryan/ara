#!/bin/bash
# shellcheck source=/dev/null
# shellcheck disable=SC2154,SC2153
# AI1WM Logic - Ara Backup Bash Script
# Location: cli/backup/copy/ai1wm-backup-logic.sh
# Version 2.0.1
# Author: Conor Ryan
# Date: 2024-02-13
# Description: This script runs AI1WM plugin backup and moves the backup to the host system
####

if [[ ${backup_enabled} != 1 ]]; then
    log "Backup is not enableded.";
    log "Illegal State: Backup Section should not be running."
    log "Aborting Script";
    exit 1;

fi

log "Starting AI1WM Backup Section"

ai1wm "${SITE_NAME}" "${APACHE_ROOT}"

if [[ -n "${SITE_NAME_2}" && "${AI1WM_ACTIVE_2}" == 1 ]]; then
    ai1wm "${SITE_NAME_2}" "${APACHE_ROOT_2}"

fi

log "AI1WM Backup Section Completed"