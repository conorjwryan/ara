# Ara Backup Script Troubleshooting Guide

Below are some common issues that you may encounter when setting up the Ara Backup Script. Be sure to follow look at the [First Steps](#first-steps) section as this will cover most script issues.

If you have any issues that are not covered here please open an issue on the [Ara GitLab Repository](https://gitlab.com/conorjwryan/ara/-/issues).

## Table of Contents

- [First Steps](#first-steps)
  - [Is Ara Already Installed and Working?](#is-ara-already-installed-and-working)
  - [Have You Read the Documentation and Setup the Script Correctly?](#have-you-read-the-documentation-and-setup-the-script-correctly)
  - [Have You Run the Script in Test Mode?](#have-you-run-the-script-in-test-mode)
  - [Have You Checked the Logs?](#have-you-checked-the-logs)
- [Other Issues](#other-issues)
  - [Why is The Script Only Transferring Encrypted Files to Remote?](#why-is-the-script-only-transferring-encrypted-files-to-remote)
  - [How can I Encrypt and Transfer the Entire Backup Directory?](#how-can-i-encrypt-and-transfer-the-entire-backup-directory)
  - [My Remote Transfer Has Failed, What Do I Do?](#my-remote-transfer-has-failed-what-do-i-do)

## First Steps

### Is Ara Already Installed and Working?

This script assumes that you have already set up the Ara Docker Environment. If you have not done so please refer to the [README.md](../../README.md) file in the root of this repository for instructions on how to do so.

### Have You Read the Documentation and Setup the Script Correctly?

The backup script requires extra environmental variables to be set up in your `.env` file. These are not set up by default. Please refer to the `.env` file in the root of this repository for an example of the required variables as well as the [README.md](README.md) file in the `cli/backup` directory for more information.

### Have You Run the Script in Test Mode?

The script has two test modes which will check that the script is set up correctly. This is done by running the following command:

```bash
./cli/backup/backup.sh test env
```

This will check that the environmental variables are set up correctly. If this passes you can then run the following command to then check which directories you want to be backed up in the created tarball:

```bash
./cli/backup/backup.sh test backup
```

### Have You Checked the Logs?

The script logs all of its actions to a log file in the `logs` directory of the script. This is useful for debugging and monitoring purposes. Each time the script is run this generates a new log file named `ara-backup-log_<date>_<time>`.

## Other Issues

### Why is The Script Only Transferring Encrypted Files to Remote?

This is by design and follows security `best practices`. If you transfer non-encrypted files to remote public cloud then you are exposing your data to the remote server and any one who has access to it. If the remote server is compromised then your data is easily accessible.

Encryption is a way to mitigate this risk by requiring a key to decrypt the data. This means that even if the remote server is compromised the data is still encrypted and cannot be read.

If you want to transfer other files to remote you can do so by running the script in `encrypt-and-transfer` mode. This will encrypt and transfer all files in the backup directory to remote.

```bash
./cli/backup/backup.sh encrypt-and-transfer
```

### How can I Encrypt and Transfer the Entire Backup Directory?

Normal operations (`default`, `complete`, `no-uploads`) will only encrypt and transfer files in the dated directory. This is by design to ensure the backup is a consistent size and speed.

If you want to encrypt and transfer other files to remote as a one-off you can do so by running the script in `encrypt-and-transfer` mode. This will encrypt and transfer all files in the backup directory to remote.

```bash
./cli/backup/backup.sh encrypt-and-transfer
```

Alternatively you can explicitly set the `ENCRYPT_ALL` variable to `1` in the `Backup Options` section of the `.env` file. This will encrypt and transfer all files in the backup directory to remote during a normal backup script run.

### My Remote Transfer Has Failed What Do I Do?

If the remote transfer has failed then you should check the logs to see what the issue is. These are located in the `logs` directory of the script.

If the script lost connection during the transfer then you can simply run the script in `transfer-only` mode and it will pick up where it left off.

```bash
./cli/backup/backup.sh transfer-only
```
