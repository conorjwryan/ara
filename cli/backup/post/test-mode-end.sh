#!/bin/bash
# shellcheck source=/dev/null
# shellcheck disable=SC2154,SC2034,SC2181
# Cleanup Section Logic - Ara Backup Bash Script
# Location: cli/backup/post/test-mode-end.sh
# Version 1.0.2_1
# Author: Conor Ryan
# Date: 2024-02-13
# Description: This script houses the logic for test mode in the script clean up section
####

if [[ $1 == "test" ]]; then
		log "INFO: Test Mode Enabled";
		echo "";

		if [[ $2 == "env" ]]; then
			log "Script Complete!";
			log "If you see this message the script runs to the end.";
			echo "";
			log "Please check the output above for any 'INFO:' messages";
			echo "";
			log "If you have not already run 'test backup' do so now to ensure the backup function works as expected";
			log "'backup.sh test backup'";
			echo "";
			log "Once all tests are complete without errors you can now safely run the script in default mode.";
			echo "";
			log "Finished Backup Script at $(date +%Y-%m-%d\ %H:%M:%S)";
			exit 0;

		elif [[ $2 == "backup" ]]; then
			ls -lh "${BACKUP_DIR}${date}";

			echo "";
			log "Script Complete!";
			log "3 backup tar files have been placed in your stated backup directory";
			log "${BACKUP_DIR}";
			echo "";
			log "Unarchive them to ensure they contain the expected information";
			log "Finished Backup Script at $(date +%Y-%m-%d\ %H:%M:%S)";
			exit 0;

		fi

fi