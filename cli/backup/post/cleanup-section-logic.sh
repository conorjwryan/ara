#!/bin/bash
# shellcheck source=/dev/null
# shellcheck disable=SC2154,SC2034,SC2181
# Cleanup Section Logic - Ara Backup Bash Script
# Location: cli/backup/post/cleanup-section-logic.sh
# Version 2.0.0
# Author: Conor Ryan
# Date: 2023-12-18
# Description: This script is used to cleanup the backup directory after a successful transfer
####

####
# Test Mode End Section
####

source ./post/test-mode-end.sh

####
# Post Transfer Cleanup Section
####

log "Beginning the Script Cleanup Process";

if [[ ${transfer_success} -gt 0 || ${transfer_partial_success} -gt 0 ]]; then
	log "Transfer of Encrypted Files to Remote(s) was Successful"; 

	if [[ ${CRON_ACTIVE} == 1 ]]; then
			log "Sending Heartbeat to Cron Server";
			curl "${CRON_BACKUP_URL}";

	fi

	# Removing Encrypted Files from Backup Directory
	find "${BACKUP_DIR}" -type f -name "*.asc" -print -delete > /dev/null 2>&1;
	if [[ $? == 0 ]]; then
		log "Encrypted Files Removed from Backup Directory";

	else 
		log "ERROR: Encrypted Files not Removed from Backup Directory";
		log "ERROR: Please check the logs for more information";
		exit 1;

	fi 

else
	log "ERROR: Transfer Failed";
	log "ERROR: Backup Directory Contents will not be removed";
	log "ERROR: Please check the logs for more information";
	exit 1;

fi

	find "${BACKUP_DIR}" -type d -empty -delete > /dev/null 2>&1;
	mkdir -p "${BACKUP_DIR}";

	log "Empty Directories Removed";

if [[ ${transfer_partial_success} -gt 0 ]]; then
	log "INFO: There were some files that were not encrypted and were not transferred";
	tree -pufi "${BACKUP_DIR}";

fi

log "Finished Backup Script at $(date +%Y-%m-%d\ %H:%M:%S)";
exit 0;