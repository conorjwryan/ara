# Log Folder Information

---

Location: ara/cli/backup/logs

---

The logs folder contains the logs which are generated when the backup script `cli/backup/backup.sh` is run. The logs are generated in the following format:

```text
ara-backup-log_<date>_<time>.log
```

The logs contain the output of the script when it is run. This is useful for debugging and monitoring purposes.

## Note About Log Retention

The logs are not automatically deleted. It is recommended that you delete the logs after a certain period of time. This can be done by running the following command:

```bash
find cli/backup/logs/ -type f -name "*.log" -mtime +<days> -exec rm {} \;
```

Where `<days>` is the number of days you want to keep the logs for. For example, if you want to keep the logs for 30 days you would run:

```bash
find cli/backup/logs/ -type f -name "*.log" -mtime +30 -exec rm {} \;
```

In the above example it assumes you run the command from the `ARA` root directory.
