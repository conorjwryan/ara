#!/bin/bash
# shellcheck source=/dev/null
# shellcheck disable=SC2154,SC2181,SC2034
# Main Encryption Logic - Ara Backup Bash Script
# Location: cli/backup/encryption/main-encryption-logic.sh
# Author: Conor Ryan
# Date: 2023-12-20
# Description: This script deals with the main encryption logic for the encryption section of the Ara Backup Bash Script
####

shopt -s globstar nullglob
for file in "${encryption_dir}"**/*; do

	# Skip if file is a directory
	if [[ -d $file ]]; then
		log "INFO: Skipping $file as it is a directory";
		echo "";
		continue;

	fi

	# Work on files that are not already encrypted
	if [[ $file != *.asc ]]; then
		log "Processing $file";

		gpg --encrypt --armor  --recipient "${ENCRYPT_GPG_EMAIL}" "$file"
		if [[ $? -ne 0 ]]; then
			log "ERROR: Failed encrypting $file"
			((encryption_failures++))
			continue;

		fi
		
		((files_encrypted++))
		log "Removing unencrypted $file";
		echo "";

		rm -rf "$file"
        if [[ $? -ne 0 ]]; then
            log "ERROR: Failed removing $file"
			((removal_failures++))
            continue;

        fi

	else 
		# Skip if file is already encrypted
		log "INFO: Skipping $file as it is already encrypted";
		echo "";
		((files_skipped++))

	fi

done
