#!/bin/bash
# shellcheck source=/dev/null
# shellcheck disable=SC2154,SC2181,SC2034
# Encryption Section Logic - Ara Backup Bash Script
# Location: cli/backup/encryption/encrpytion-section-logic.sh
# Version 4.0.0
# Author: Conor Ryan
# Date: 2023-12-20
# Description: This script is the encryption section of the Ara Backup Bash Script
####

if [[ ${encrypt_enabled} != 1 ]]; then
    log "Encryption is not enableded.";
    log "Illegal State: Encryption Section should not be running."
    log "Aborting Script";
    exit 1;

fi

source ./encryption/encryption-validation-logic.sh

log "Starting Encryption";
echo "";

encryption_failures=0
removal_failures=0
files_encrypted=0
files_skipped=0

source ./encryption/main-encryption-logic.sh

source ./encryption/post-encryption-logic.sh

log "Encryption Section Completed";
echo "";