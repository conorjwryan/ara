#!/bin/bash
# shellcheck source=/dev/null
# shellcheck disable=SC2154,SC2181,SC2034
# Post Encryption Logic - Ara Backup Bash Script
# Location: cli/backup/encryption/post-encryption-logic.sh
# Author: Conor Ryan
# Date: 2023-12-20
# Description: This script deals with the post encryption logic for the encryption section of the Ara Backup Bash Script
####

# Inform of encryption results
if [[ ${files_encrypted} -gt 0 ]]; then
	log "INFO: ${files_encrypted} file(s) were encrypted";

else
	log "INFO: No files were encrypted";

fi

if [[ ${files_skipped} -gt 0 ]]; then
	log "INFO: ${files_skipped} file(s) were skipped";

fi

echo "";

if [[ ${encryption_failures} -gt 0 || ${removal_failures} -gt 0 ]]; then

	if [[ ${encryption_failures} -gt 0 ]]; then
		log "ERROR: There were ${encryption_failures} failure(s) during encryption"

	fi

	if [[ ${removal_failures} -gt 0 ]]; then
		log "ERROR: There were ${removal_failures} failure(s) during removal of unencrypted files"

	fi

    tree -pufi "${BACKUP_DIR}";
	exit 1;

fi