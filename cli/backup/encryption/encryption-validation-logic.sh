#!/bin/bash
# shellcheck source=/dev/null
# shellcheck disable=SC2154,SC2181,SC2034
# Encryption Validation Logic - Ara Backup Bash Script
# Location: cli/backup/encryption/encryption-validation-logic.sh
# Version 1.0.0
# Author: Conor Ryan
# Date: 2023-12-20
# Description: This scfipt deals with the validation logic for the encryption section of the Ara Backup Bash Script
####

####
# Assumes that the GPG key is already imported into the host system
# Checks for existance of public key at the start of the script
####

if [[ ${ENCRYPT_ALL} == 1 ]]; then

	log "INFO: 'ENCRYPT_ALL' variable is Enabled";
	log "Encrypting all files in ${BACKUP_DIR} directory";
	echo "";
	encryption_dir="${BACKUP_DIR}"

else

	if [[ ${backup_enabled} != 1 ]]; then
		log "Encrypting all files in ${BACKUP_DIR} directory"
		echo "";
		encryption_dir="${BACKUP_DIR}"
		

	else
		log "Backup Process is Running.";
		log "Encrypting files in ${BACKUP_DIR}$date/ directory only"
		echo "";
		encryption_dir="${BACKUP_DIR}$date/"

		non_asc_files=$(find "${BACKUP_DIR}" -path "${BACKUP_DIR}${date}" -prune -o -type f ! -name "*.asc" -print)

		if [[ -n $non_asc_files ]]; then
			log "INFO: Non-encrypted files were found outside of the dated backup directory"
			log "INFO: This could indicate that the backup process failed previously."
			log "INFO: Please check the backup logs for more information"
			log "INFO: These files will be skipped during encryption"
			log "INFO: To encrypt these extra files in the Backup Directory run this script in 'encrypt-and-transfer' mode"
			echo "";
		fi

	fi

fi

if [[ ! -d "${encryption_dir}" ]]; then
	log "Directory ${encryption_dir} does not exist, aborting encryption";
	exit 1;

fi