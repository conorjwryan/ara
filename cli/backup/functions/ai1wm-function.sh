#!/bin/bash
# shellcheck source=/dev/null
# shellcheck disable=SC2154
# AI1WM Logic - Ara Backup Bash Script
# Location: cli/backup/function/ai1wm-function.sh
# Version 1.0.0
# Author: Conor Ryan
# Date: 2024-02-13
# Description: This script runs AI1WM plugin backup function
####
ai1wm() {

    local site_name="${1}";
    local apache_root="${2}";

    log "\e[32m$1\e[0m Site";
    log "Removing .wpress From Backups"

    docker exec -i "${site_name}_wp" find /var/www/"${apache_root}"/wp-content/ai1wm-backups -type f -delete

    ####
    # Actual AI1WM Backup Process
    ####

    log "Backing Up AI1WM";

    docker exec -i --user www-data "${site_name}_wp" wp ai1wm backup --path='/var/www/'"${apache_root}"'/';

    ####
    # AI1WM Backup Output Transfer
    ####

    log "Moving .wpress Output on WordPress Container";

    docker exec -i "${site_name}_wp" find /var/www/"${apache_root}"/wp-content/ai1wm-backups -name '*.wpress' -exec mv {} /var/backups/"$(date +%Y-%m-%d_%H-%M-%S)"_"${site_name}".wpress \;

    log "Moving .wpress Output on Host System";

    find "${ARA_HOME}docker/wordpress/backups/" -name '*.wpress' -exec mv {} "${BACKUP_DIR}${date}/" \;

}
