#!/bin/bash
# shellcheck source=/dev/null
# shellcheck disable=SC2154
# Backup Functions Logic - Ara Backup Bash Script
# Location: cli/backup/functions/backup-tar-function.sh
# Version 1.1.0
# Author: Conor Ryan
# Date: 2024-02-13
# Description: This script includes the functions needed for the backup script
####

source ./functions/log-process-function.sh

source ./functions/backup-tar-function.sh

source ./functions/ai1wm-function.sh