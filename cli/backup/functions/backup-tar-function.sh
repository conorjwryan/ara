#!/bin/bash
# shellcheck source=/dev/null
# shellcheck disable=SC2154
# Backup Tar Function - Ara Backup Bash Script
# Location: cli/backup/functions/backup-tar-function.sh
# Version 1.1.0
# Author: Conor Ryan
# Date: 2024-02-13
# Description: This script includes the function which creates the tarball based on script mode  and moving it to the host system
####

function backup_site() {
    # Get the excluded directories as an argument
    local excluded_dirs=("$@")

    local site_name=${excluded_dirs[-3]}
    local apache_root=${excluded_dirs[-2]}
    local suffix=${excluded_dirs[-1]}
    
    unset 'excluded_dirs[${#excluded_dirs[@]}-3]'
    unset 'excluded_dirs[${#excluded_dirs[@]}-2]'
    unset 'excluded_dirs[${#excluded_dirs[@]}-1]'  

    docker exec -i "${site_name}_wp" cp -a "/var/www/${apache_root}" /var/backups/

    # Define the directory
    local content_dir="/var/backups/${apache_root}/wp-content"

    # Build the find command
    local find_cmd="docker exec -i \"${site_name}_wp\" find \"${content_dir}\" -mindepth 1"
    for excluded_dir in "${excluded_dirs[@]}"; do
        find_cmd+=" \( -path '${content_dir}/${excluded_dir}' -prune \) -o"
    done

    # Add the -exec option to delete the files
    find_cmd+=" -type f -exec rm -f {} \;"

    # Run the command
    eval "${find_cmd}"

    docker exec -i "${site_name}_wp" tar -czpf "/var/backups/${datetime}_${site_name}_${suffix}.tgz"  "/var/backups/${apache_root}/";

    return 
}

