#!/bin/bash
# shellcheck source=/dev/null
# Backup Log Process Function - Ara Backup Bash Script
# Location: cli/backup/functions/log-process-function.sh
# Version 1.1.2_1
# Author: Conor Ryan
# Date: 2024-02-13
# Description: This script includes the log setup and functions needed for the backup script
####

if [ ! -d "logs" ]; then
    mkdir logs

    if [ ! -d "logs" ]; then
        echo -e "Error: Unable to create logs directory"
        echo -e "Error: Check permissions and try again"
        exit 1

    fi

fi

log_name=$(date '+%Y-%m-%d_%H-%M-%S')

if [ ! -f "logs/ara-backup-log_${log_name}}.log" ]; then
        touch "logs/ara-backup-log_${log_name}.log"

        if [ ! -f "logs/ara-backup-log_${log_name}.log" ]; then
            echo -e "Error: Unable to create log file"
            echo -e "Error: Check permissions and try again"
            exit 1

        fi

fi

log() {
    log_file="logs/ara-backup-log_${log_name}.log"

    echo -e "$1";
    echo "$(date '+%Y-%m-%d %H:%M:%S') - $1" >> "${log_file}"

}
