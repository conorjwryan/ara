# Backup Script Information

---

Location: ara/cli/backup

---

The backup script is used to backup the websites and databases of the ARA project. It is designed to be run on a cron job and will backup the websites and databases to a specified directory, encrypt and then transfer them to a remote server. The script is modularised and can be run in different modes depending on the desired outcome.

## Before You Begin

### Make Files Executable (If Not Already)

Before the script is run you must ensure that the files are executable. This can be done by running the following command (assuming from the ARA root directory):

```bash
find cli/ -type f -name "*.sh" -exec chmod +x {} \;
```

Because this bash script is modularised it is important that the files are executable so that they can be run. If you do not do this then the script will not run.

### Script Pre-Requisites

This script assumes that you have already set up the an ARA instance and have a running instance and that you have the following:

- a `docker-compose.yml`
- an `.env` file
- `ARA_HOME` directory defined in `.bashrc`
- A `GPG` key created for the server (for encryption)
- A remote server set up with access via `rclone`/`rsync` or both

### Script Setup Steps (Environment Variables)

In order to run the script you must first set up the following variables in the `.env` file:

- `BACKUP_DIR` - the directory where the backups will be stored locally on the server for processing
- `ENCRYPT_GPG_EMAIL` - the email address associated with the GPG key used for encryption
- Set up `rclone` and/or `rsync` for remote transfer (see below)

Note that if no remote is setup the script will not run.

### Setting Up RClone and/or RSync for Remote Transfer (Required)

In order to transfer the backups to a remote server you must set up either `rclone` or `rsync` or both. This is done by setting up the following variables in the `.env` file:

#### RClone (to Remote Storage Provider)

- `RCLONE_ACTIVE` - set to `1` if you want to use `rclone` for remote transfer
- `RCLONE_REMOTE` - the name of the remote server as defined in `rclone`
- `RCLONE_DIR` - the directory on the remote server where the backups will be stored (bucket first)

#### RSync (Remote Server via SSH)

- `RSYNC_ACTIVE` - set to `1` if you want to use `rsync` for remote transfer
- `RSYNC_SSH` - the SSH connection string for the remote server (eg: `user@remote-server.com`)
- `RSYNC_DIR` - the directory on the remote server where the backups will be stored

### Optional Variables to Set

The following variables are optional and can be set in the `.env` file:

- AI1WM_ACTIVE - set to `1` if you want to use All-in-One WP Migration for backups (Requires unlimited extension plugin to be installed)
- `ENCRYPT_ALL` - set to `1` if you want to encrypt all files in the backup directory (not just the `.tar.gz` and `.sql` files in the dated directory)
- `CRON_ACTIVE` - set to `1` if you want to send a `cURL` request to a specified URL when the backup is complete
- `CRON_BACKUP_URL` - the URL to send a `cURL` request to when the backup is complete

## Script Process (Default Mode)

The script runs through the following process in its default mode (blank or `default` mode):

```bash
./cli/backup/backup.sh
```

1. Checks for presence of `.env` file and that the required variables are set
2. Creates a dated directory in the backup location specified in `.env`
3. Runs a `ai1wm` backup of the site (if the site has the plugin installed)
4. Runs a database dump command, which creates a separate backup of the databases.
5. Creates a tarball of the site files (see below for more information)
6. Encrypts these 1-3 using gpg and a specified key
7. Transfers the encrypted files to a remote server using rsync and rclone
8. Cleanup - Removes the (presumably) empty dated directory.
9. Sends a `cURL` request to a specified URL to notify that the backup has been completed - this is used for monitoring purposes (optional)

Note: In `default` mode if other non-encrypted files are present in the backup directory they will not be encrypted or transferred. This is by design. See the section on [Transferring Non-Encrypted Files to Remote](#transferring-non-encrypted-files-to-remote) for more information.

### Logging

The script logs all of its actions to a log file in the `logs` directory. This is useful for debugging and monitoring purposes. Each time the script is run this generates a new log file named `ara-backup-log_<date>_<time>`.

## First Run

Once you have set up the required files and variables it is now important to test that the script is working correctly. There are currently two tests you should run, in oder these are:

1. `backup.sh test env`
2. `backup.sh test backup`

### Test Environment Variables (ENV)

This first test is to check that the environmental variables are set up correctly. This is done by running the following command:

```bash
./cli/backup/backup.sh test env
```

This checks the environmental variables and runs checks for `rsync` and `rclone` that if they are enabled that the script runs without error. Pay attention to any messages in the terminal which begin with `INFO:` as these are important.

### Backup Test

Once the `test env` test has been run with success you can safely run the Backup Test. This goes through the motions creating compressed backups of the WordPress and database files without encryption or transfer. These files are put in a dated directory in the backup location specified in `.env` in order for you to check which backup you are looking for.

The test creates tarballs of the 3 available options for backup:

1. Core WP Files and WP-Content Directories - (WP content files only `plugins`, `themes`, `uploads` - no extra WP-Content directories like `updraft` or `ai1wm-backups) - largest size
2. Complete including all WP-Content Directories - (plugins, themes, uploads) - medium size
3. 'No Uploads' - (plugins, themes) - smallest size

The idea here is that based on whether you have a large amount of media or extra `WP-Content` files (caused by backup plugins) you can choose which backup you want to run. The default is option 1.

```bash
./cli/backup/backup.sh test backup
```

## Running the Script

Once you have run the tests and are happy that the script is working correctly you can now run the script.

```bash
./cli/backup/backup.sh `<mode>`
```

### Backup Modes

The script can be run with the following options:

- **default** (or blank) - runs the backup, encryption and transfer processes (wordpress and core `wp-content` files only)

- **complete** - runs the backup, encryption and transfer processes (complete archive of site)

- **no-uploads** - runs the backup, encryption and transfer processes (no uploads tarball)

### Special Modes

- **encrypt-and-transfer** - runs the encryption and transfer processes of the specified backup directory only - useful if you have other non-encrypted files you want to transfer

- **transfer-only** - runs the transfer process of the specified backup directory only - useful if transfer fails and you want to retry without going through the default process

### Other Modes

- **test** - runs the test process (see above)

- **help** - displays the help menu

## Upcoming Features

- Ability to have multiple instances of `rsync` and `rclone` for different backup locations
- Setup Script - The script will have a setup script which will set up the project and the script for you.
- In-depth Setup Guide - The script will have a written setup guide which will explain how to set up the project and script.
- Decrypt and Restore Companion Script - The script will have a mode which will decrypt and restore the backups.

## Known Issues and Limitations

- This script does not allow by default a 'backup only mode', where backups aren't then encrypted and transferred. This is due to security concerns and best practices
- The script does not currently allow for multiple instances of `rsync` and `rclone` for different backup locations (likely to change in future)
- The script is not currently able to successfully transfer backups via RSync to a Mac due to differences between Linux and Mac versions
- This script is not able to decrypt/restore backups.

### Transferring Non-Encrypted Files to Remote

This script by design does not allow transfer of non-encrypted files (be they created through the script or not) to a remote server. This is due to security concerns and best practices.

Only `.asc` files count as encrypted and are thus transferred.

If you would like to encrypt and transfer files that were not created by the script you can use the `encrypt-and-transfer` mode. This will encrypt and transfer the contents specified backup directory (`BACKUP_DIR`) to the remote server regardless of whether the files were created by the script or not.

### Multiple WP Single DB Setups

If you have multiple `WordPress` sites with a single database the script will sense this and create an extra `tarball` but this will only work for `SITE_2` only.

You can set up the following extra variables in the `.env` file for extra backup options for the second site:

- `AI1WM_ACTIVE_2` - set to `1` if you want to use All-in-One WP Migration for backups for the second site (Requires unlimited extension plugin to be installed).

- `RSYNC_UPLOADS_ACTIVE_2` - set to `1` if you want to use sync for remote transfer for the second site

- `RSYNC_UPLOADS_DIR_2` - the directory on the remote server where the upload copies will be stored for the second site

## Troubleshooting

If you have any issues with the script please refer to the [Troubleshooting Guide](TROUBLESHOOT.md).
