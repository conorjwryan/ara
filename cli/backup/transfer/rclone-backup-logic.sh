#!/bin/bash
# shellcheck disable=SC2181,SC2034,SC2154
# shellcheck source=/dev/null
# RClone Backup Logic - Ara Backup Bash Script
# Location: cli/backup/transfer/rclone-backup-logic.sh
# Version 2.1.2
# Author: Conor Ryan
# Date: 2023-12-17
# Description: This script is used to transfer the encrypted backups to an S3 Bucket via RClone
####

if [[ $transfer_enabled != 1 ]]; then
    log "Transfer is not enabled.";
    log "Illegal State: Transfer Section should not be running."
    log "Aborting Script";
    exit 1;

fi

if [[ ${RCLONE_ACTIVE} != 1 ]]; then
    log "RClone is not active.";
    log "Illegal State: RClone Section should not be running."
    log "Aborting Script";
    exit 1;

fi

####

log "Rclone Transfer Section is Enabled";

log "Transfering to Remote via RClone ${RCLONE_REMOTE}";
echo "";

rclone copy --verbose "${BACKUP_DIR}" "${RCLONE_REMOTE}${RCLONE_DIR}" --include "*.asc" --log-file "${LOG_FILE}"

if [[ $? -eq 0 ]]; then

    if [[ $rclone_transfer_skipped -gt 0 ]]; then
        log "INFO: RClone Transfer Partially Successful";
        log "INFO: Non-encrypted files were skipped"
        echo "";
        ((transfer_partial_success++));

    else
        log "RClone Transfer Successful";
        echo "";
        ((transfer_success++));
        ((rclone_transfer_success++))

    fi

else
    log "INFO: RClone Transfer failed";
    echo "";
    ((transfer_failed++));
    ((rclone_transfer_failed++))
            
fi