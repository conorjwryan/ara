#!/bin/bash
# shellcheck source=/dev/null
# shellcheck disable=SC2154,SC2181
# RSync Backup Logic - Ara Backup Bash Script
# Location: cli/backup/transfer/rsync-backup-logic.sh
# Version 2.2.0
# Author: Conor Ryan
# Date: 2024-02-13
# Description: This script is used to transfer the encrypted backups to a remote server via RSync
####

if [[ $transfer_enabled != 1 ]]; then
    log "Transfer is not enabled.";
    log "Illegal State: Transfer Section should not be running."
    log "Aborting Script";
    exit 1;

fi

if [[ ${RSYNC_ACTIVE} != 1 ]]; then
    log "RSync is not active.";
    log "Illegal State: RSync Section should not be running."
    log "Aborting Script";
    exit 1;

fi

####
# Transfer WordPress Uploads Folder
####

if [[ ${RSYNC_UPLOADS_ACTIVE} == 1 ]]; then
    log "Syncing WordPress Uploads Folder in ${SITE_NAME} via Rsync";
    echo "";

    rsync -rvXDP --ignore-existing "${ARA_HOME}docker/wordpress/sites/${SITE_NAME}/uploads/" "${RSYNC_SSH}:${RSYNC_UPLOADS_DIR}";

    if [[ $? -eq 1 ]]; then
        log "ERROR: RSync Uploads Failed";
        echo "";
        ((transfer_failed++));
        ((rsync_transfer_failed++));

    else
        log "RSync Uploads Successful";
        echo "";

        ((transfer_success++));

    fi

else
    log "RSync Uploads is not active. Skipping RSync transfer.";
    echo "";

fi

# Temporary Include if RSync for Site 2 uploads is active
if [[ ${RSYNC_UPLOADS_ACTIVE_2} == 1 ]]; then
    log "Syncing WordPress Uploads Folder in ${SITE_NAME_2} via Rsync";
    echo "";

    rsync -rvXDP --ignore-existing "${ARA_HOME}docker/wordpress/sites/${SITE_NAME_2}/uploads/" "${RSYNC_SSH}:${RSYNC_UPLOADS_DIR_2}";

    if [[ $? -eq 1 ]]; then
        log "ERROR: RSync Uploads Failed";
        echo "";
        ((transfer_failed++));
        ((rsync_transfer_failed++));

    else
        log "RSync Uploads Successful";
        echo "";

        ((transfer_success++));

    fi

else
    if [[ -n ${SITE_NAME_2} ]]; then
        log "RSync Uploads for  2 is not active. Skipping RSync transfer.";
        echo "";

    fi

fi

####
# Transfer Encrypted Backups
####

log "Transferring to Remote via RSync";
echo "";

rsync --bwlimit=5000 -rvXD --ignore-existing --include="*/" --include="*.asc" --exclude="*" "${BACKUP_DIR}" "${RSYNC_SSH}":"${RSYNC_DIR}";

if [[ $? -eq 0 ]]; then

    if [[ $rsync_transfer_skipped -gt 0 ]]; then
        log "INFO: RSync Transfer Partially Successful";
        log "INFO: Non-encrypted files were skipped";
        echo "";
        ((transfer_partial_success++));

    else
        log "RSync Transfer Successful";
        echo "";
        ((transfer_success++));
        ((rsync_transfer_success++))

    fi

else
    log "INFO: RClone Transfer failed";
    echo "";
    ((transfer_failed++));
    ((rsync_transfer_failed++))

fi