#!/bin/bash
# shellcheck source=/dev/null
# shellcheck disable=SC2154,2034
# Transfer Section Logic - Ara Backup Bash Script
# Location: cli/backup/transfer/transfer-section-logic.sh
# Version 2.0.1
# Author: Conor Ryan
# Date: 2023-12-17
# Description: This script is the basis of the transfer section of the Ara Backup Bash Script
####

if [[ ${transfer_enabled} != 1 ]]; then
    log "Transfer is not enableded.";
    log "Illegal State: Transfer Section should not be running."
    log "Aborting Script";
    exit 1;

fi

####
# Transfer Section
####
# This section will transfer the backups to a remote server
# The backups will be transferred via RClone and RSync
# RClone will be used to transfer the encrypted backups to an S3 Bucket
# RSync will be used to transfer the encrypted backups to a remote server
####

####
# Set to Default Values
####

rclone_transfer_failed=0
rclone_transfer_skipped=0
rclone_transfer_success=0
rsync_transfer_failed=0
rsync_transfer_skipped=0
rsync_transfer_success=0

####

log "Beginning Transfer Process";
echo "";
find_non_asc=$(find "${BACKUP_DIR}" -type f ! -name "*.asc")

if [[ -n $find_non_asc ]]; then
    log "INFO: The following non-asc files will be skipped:"
    log "$find_non_asc"
    echo "";
    ((rclone_transfer_skipped++))
    ((rsync_transfer_skipped++))

fi

####
# Transfering Backup to Remote via RClone
####
# This section will transfer the encrypted backups to an S3 Bucket
####

if [[ "${RCLONE_ACTIVE}" == 1 ]]; then
    source ./transfer/rclone-backup-logic.sh

else 
    log "INFO: RClone Transfer is not active, skipping..."
    echo "";

fi

####
# Transfering to Remote via Rsync
####

if [[ "${RSYNC_ACTIVE}" == 1 ]]; then
    source ./transfer/rsync-backup-logic.sh

else 
    log "INFO: RSync Transfer is not active, skipping..."
    echo "";

fi

####
# End of Transfer Section
####

log "End Transfer Process";
echo "";
