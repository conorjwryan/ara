#!/bin/bash
# shellcheck source=/dev/null
# shellcheck disable=SC2034,SC2181
# RSync Check Logic - Ara Backup Bash Script
# Location: cli/backup/pre/rsync-check-logic.sh
# Version 1.0.4
# Author: Conor Ryan
# Date: 2023-12-17
# Description: This script is used to check the prerequisites are met before running the backup script
####

if [[ ${RSYNC_ACTIVE} != 1 ]]; then
	log "RSync is not active.";
	log "Illegal State: RSync Section should not be running."
	log "Aborting Script";

	exit 1;
	
fi

log "Checking for RSync Remote Path";

touch "${BACKUP_DIR}test.txt";

rsync --bwlimit=5000 -rvXD --ignore-existing --remove-source-files "${BACKUP_DIR}" "${RSYNC_SSH}":"${RSYNC_DIR}" > /dev/null 2>&1;

rm "${BACKUP_DIR}test.txt";

if [[ $? == 0 ]]; then
	log "RSync Remote Path Found";
	echo "";

else
	log "INFO: RSync Remote Path Not Found or RSync failed";
	log "Please check the 'RSYNC_SSH' and 'RSYNC_DIR' variables in the '.env' file.";
	log "If they are correct please ensure you have created the directory on the remote before running this script."
	log "RSync Transfer cannot be done.";
	echo "";

	RSYNC_ACTIVE=0;
	return;

fi