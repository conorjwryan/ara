#! /bin/bash
# shellcheck source=/dev/null
# shellcheck disable=SC2034,SC2154
# Directory / File Logic - Ara Backup Bash Script
# Location: cli/backup/pre/script-prerequisite-logic.sh
# Version 1.0.0
# Author: Conor Ryan
# Date: 2023-12-19
# Description: This script is used to specifically check for the existance of required directories and files before running the backup script
####


####
# Check Existance of Required Directories
####

date=$(date +%Y-%m-%d);
datetime=$(date +%Y-%m-%d_%H-%M-%S)
datetime_echo=$(date +%Y-%m-%d\ %H:%M:%S)

log "The Date/Time is ${datetime_echo}";
log "Checking for existance of required directories"

####
# Check Existance of Backup Directory
####

if [[ ! -d "${BACKUP_DIR}" ]]; then
	log "INFO: Backup Directory does not exist. Creating...";
	echo "";

	mkdir "${BACKUP_DIR}";

	if [[ ! -d "${BACKUP_DIR}" ]]; then
		log "ERROR: Backup Directory could not be created. Aborting script.";
		log "ERROR: Please check the permissions of the containing directory and try again.";
		exit 1;

	fi

else
	log "INFO: Backup Directory exists";

fi

####
# Check Existance of Date Directory
####

if [[ ${backup_enabled} == 1 ]]; then

	log "Creating Date Folder '$date' in Backup Directory";

	if [[ ! -d "${BACKUP_DIR}$date" ]]; then
		mkdir "${BACKUP_DIR}$date";
		
		if [[ ! -d "${BACKUP_DIR}$date" ]]; then
			log "Date Folder '$date' could not be created. Aborting script.";
			log "Please check the permissions of the Backup Directory and try again.";
			exit 1;

		fi
		
		log "Date Folder '$date' created";
		backup_dir_creation=1

	else
		log "Directory already exists";

	fi

else
    if [[ $1 == "encrypt-and-transfer" || $1 == "transfer-only" ]]; then
        backup_dir_creation=0
        echo "";
        log "INFO: '$1' mode selected.";
        log "INFO: Backup is disabled.";
        echo "";
        log "Checking Backup Directory is not empty before proceeding...";
        echo "";

        if [[ "$(ls -A "${BACKUP_DIR}")" ]]; then
            log "Files have been found in Backup Directory. Proceeding...";
            echo "";

        else
            log "ERROR: Backup Directory '${BACKUP_DIR}' is empty. Aborting script.";
            exit 1;

        fi

    fi

fi
