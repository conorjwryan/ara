#!/bin/bash
# Argument Validation Logic - Ara Update Bash Script
# Location: cli/backup/pre/argument-valiation-logic.sh
# Version 1.2.0
# Author: Conor Ryan
# Date: 2023-12-17
# Description: This script will validate the arguments passed to the script - includes help section
# shellcheck disable=SC2034
####

####
# Check for Script Mode
####
# This section will check for the existance of the $1 variable
# If the variable is set, the script will run in the mode specified unless the mode is invalid
# If the variable is not set, the script will run in default mode
####

backup_dir_creation=0
backup_enabled="";
backup_mode=""
encrypt_enabled="";
transfer_enabled="";
transfer_success=0

## testing section
if [[ $1 == "test" && $2 == "env" ]]; then
	log "Test Mode: Environemntal Variable Check";
	log "No backup, encryption or transfer will be performed";
	echo "";

	backup_enabled=0;
	encrypt_enabled=0;
	transfer_enabled=0;
	transfer_success=0;

elif [[ $1 == "test" && $2 == "backup" ]]; then
	log "Test Mode: Backup Only Mode";
	log "No encryption or transfer will be performed";
	echo "";

	backup_enabled=1;
	backup_mode="all"
	encrypt_enabled=0;
	transfer_enabled=0;
	transfer_success=0;	

elif [[ $1 == "test" && -z "$2" ]]; then
	log: "Test Mode: Help";
	echo -e "Use this mode to test the script";
	echo -e "backup.sh test <mode>";
	echo -e "Options:";
	echo -e "";
	echo -e "test backup";
	echo -e "For testing the backup function works correctly";
	echo -e "Not available as standalone option for secutiy reaons";
	echo -e "Performs 3 tar backups. 1. complete backup 2. no uploads backup 3. core backup.";
	echo -e "";
	echo -e "test env";
	echo -e "For testing environmental variables are correct";
	echo -e "No backup, encryption or transfer will be performed";
	echo -e "";
	echo -e "See script README for more information";
	exit 0;

elif [[ $1 == "encrypt-and-transfer" ]]; then
	log "Encrypt and Transfer Only Mode";
	log "No backup will be performed";

	backup_enabled=0;
	backup_mode="";
	encrypt_enabled=1;
	transfer_enabled=1;

elif [[ $1 == "transfer-only" ]]; then
	log "Transfer Only Mode";
	log "No backup or encryption of files will be performed";

	backup_enabled=0;
	backup_mode="";
	encrypt_enabled=0;
	transfer_enabled=1;

elif [[ -z "$1" || $1 == "default" ]]; then
	log "Default mode: (Core Files Backup, Encrypt and Transfer)";
	echo "";

	backup_enabled=1;
	backup_mode="core"
	encrypt_enabled=1;
	transfer_enabled=1;
	

elif [[ $1 == "no-uploads" ]]; then
	log "No Uploads Mode: (Backup, Encrypt and Transfer)";
	echo "";

	backup_enabled=1;
	backup_mode="no-uploads"
	encrypt_enabled=1;
	transfer_enabled=1;
	

elif [[ $1 == "complete" ]]; then
	log -e "Complete Mode: (Backup, Encrypt and Transfer)";
	echo "";
	
	backup_enabled=1;
	backup_mode="complete"
	encrypt_enabled=1;
	transfer_enabled=1;


elif [[ $1 == "help" ]]; then
	echo -e "ARA BACKUP SCRIPT HELP";
	echo -e "";
	echo -e "Below are options available for this script.";
	echo -e "./backup.sh <mode>";
	echo -e "";
	echo -e "Options:";
	echo -e "'default' (or empty) - default mode (backup - plugins,themes,uploads), encrypt and transfer)";
	echo -e "'no-uploads' - backup (only plugins and themes), encrypt and transfer";
	echo -e "'complete' - backup all files in ARA directory (no exclusions), encrypt and transfer";
	echo -e "";
	echo -e "Special Options:"
	echo -e "'encrypt-and-transfer' - encrypt and transfer backup contents to remote";
	echo -e "'transfer-only' - transfer only backup directory contents to remote";
	echo -e "";
	echo -e "Tests:";
	echo -e "./backup.sh test <mode>";
	echo -e "'env' - run script with all modes disabled (for testing environmental variables)";
	echo -e "'backup' - run script with backup mode enabled";
	exit 0;

else
	log "Invalid Script Mode";
	echo -e "Use 'backup.sh help' for more available modes";
	exit 1;

fi