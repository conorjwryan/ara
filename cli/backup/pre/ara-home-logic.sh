#!/bin/bash
# shellcheck disable=SC1091,SC2154,SC2086
# Ara Home Logic - Ara Backup Bash Script
# Location: cli/backup/pre/ara-home-logic.sh
# Version 1.0.3
# Author: Conor Ryan
# Date: 2023-12-17
# Description: This script is used to check the existance of the ARA_HOME variable and .env flle in ARA Directory
####

####
# Check Existance of ARA_HOME variable
####
# Found in .bashrc - if not found will default to user/ara
####

log "Checking for existance of ARA Directory"

if [ -z "$ARA_HOME" ]; then
      ARA_HOME="$HOME/ara/"
	  log "INFO: ARA_HOME variable is not set. Setting to Default: $ARA_HOME";
        echo "";

else
      log "INFO: ARA_HOME variable is set to: $ARA_HOME";
      echo "";
fi

####
# Check Existance of ARA_HOME Directory
####

if [[ ! -d "$ARA_HOME" ]]; then
	log "ARA_HOME does not exist.";
      log "If you have Project Ara in another directory please refer to the documentation on how to set the 'ARA_HOME' variable.";
      log "Aborting script...";
	exit 1;
      
fi

####
# Check Existance of .env file using ARA_HOME
####

if [[ ! -f "${ARA_HOME}.env" ]]; then
    log '.env does not exist. Aborting script.'
    exit 1;
fi