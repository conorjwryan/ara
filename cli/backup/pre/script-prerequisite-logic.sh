#! /bin/bash
# shellcheck source=/dev/null
# shellcheck disable=SC2034,SC2154
# Script Prerequisite Logic - Ara Backup Bash Script
# Location: cli/backup/pre/script-prerequisite-logic.sh
# Version 1.4.0
# Author: Conor Ryan
# Date: 2023-12-20
# Description: This script is used to check the prerequisites are met before running the backup script
####

####
# Check for GPG Public Key for Encryption
####
# This section will check for the existance of the GPG public key specified in the .env file
# If the key does not exist, the script will abort
####

log "Checking for GPG Key";

if gpg --list-keys "${ENCRYPT_GPG_EMAIL}" > /dev/null 2>&1; then
	log "GPG Key found";
	echo "";

else
	log "GPG Key not found, encryption cannot be done. Aborting script.";
	exit 1;

fi

####
# Transfer Section Checks
####


if [[ $1 == "test" && $2 == "env" ]]; then

	####
	# RClone Checks
	####
	# Checks if RClone is active, existance of config, connection to remote and existance of bucket path.
	# If any of these checks fail, RCLONE_ACTIVE is set to 0 and will not be used in transfer script
	####

	if [[ ${RCLONE_ACTIVE} == 1 ]]; then
		source ./pre/rclone-check-logic.sh

	else
		log "INFO: RClone is not active";
		log "INFO: Skipping RClone Checks";
		log "INFO: Set 'RCLONE_ACTIVE=1' in the '.env' file to enable RClone if needed";
		echo "";

	fi

	####
	# RSync Checks
	####

	if [[ ${RSYNC_ACTIVE} == 1 ]]; then
		source ./pre/rsync-check-logic.sh

	else 
		log "INFO: RSync is not active";
		log "INFO: Skipping RSync Checks";
		log "INFO: Set 'RSYNC_ACTIVE=1' in the '.env' file to enable RSync if needed";
		echo "";

	fi

fi

####
# Check if any transfer method is active
####
# If no transfer method is active, the script will abort
####

if [[ ${RCLONE_ACTIVE} != 1 && ${RSYNC_ACTIVE} != 1 ]]; then
	log "No transfer method is active. Please check for any error messages above for more information. Aborting script.";
	exit 1;

fi

source ./pre/directory-file-logic.sh