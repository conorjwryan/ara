#! /bin/bash
# shellcheck source=/dev/null
# shellcheck disable=SC2034,SC2181
# RClone Check Logic - Ara Backup Bash Script
# Location: cli/backup/pre/rclone-check-logic.sh
# Version 1.2.3
# Author: Conor Ryan
# Date: 2023-12-20
# Description: This script is used to check the prerequisites are met before running the backup script
####

if [[ ${RCLONE_ACTIVE} != 1 ]]; then
	log "ERROR: RClone is not active";
    log "Illegal State: RClone Section should not be running."
    log "Aborting Script";
	exit 1;

fi
 
log "RClone Checks Starting";

####
# Check for RClone Config
####

log "1. Checking for RClone Configuration";

if [[ -f "$HOME/.config/rclone/rclone.conf" ]]; then
	log "   RClone Config found";

else
	log "INFO: RClone Config not found. RClone is not setup";
    log "Please run 'rclone config' to create the config file.";
    log "RClone Transfer cannot be done.";
    echo "";

    RCLONE_ACTIVE=0;
    return;

fi

####
# Check for RClone Remote Existence
####

log "2. Checking for RClone Remote";

rclone_remote_test=$(rclone listremotes | grep "${RCLONE_REMOTE}")

if [[ ${rclone_remote_test} != "" ]]; then
    log "   RClone Remote Found";

else
    log "INFO: RClone Remote Not Found";
    log "Please check the 'RCLONE_REMOTE' variable in the .env file.";
    log "RClone Transfer cannot be done.";
    echo "";

    RCLONE_ACTIVE=0;
    return;

fi

####
# Check for RClone Remote Path Existence
####

log "3. Checking for RClone Remote Path";

rclone_directory_test=$(rclone lsd "${RCLONE_REMOTE}${RCLONE_DIR}")

if [[ $rclone_directory_test != "" ]]; then
    log "   RClone Remote Path Found";

else
    log "INFO: RClone Remote Path Not Found";
    log "Please check the RCLONE_DIR variable in the .env file.";
    log "If it is correct please ensure you have created the directory on the remote before running this script."
    log "RClone Transfer cannot be done.";
    echo "";

    RCLONE_ACTIVE=0;
    return;
    
fi

log "INFO: RClone Checks Complete Without Failure";
echo "";