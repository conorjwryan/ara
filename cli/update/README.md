# Update Script Information

---

Location: ara/cli/update

---

The update script is used to update the project to the latest version. It pulls the latest version from the git repository and then updates the docker images and containers.

## Script Process

The script runs through the following process in its default mode:

1. Compares the hash of the current docker-compose.yml file with the hash of the set `docker-compose` example file in the `compose` directory  to see if any changes have been made to the file.

2. Assuming no changes have been made it then pulls the latest version of the project from the git repository.

3. It then overwrites the `docker-compose.yml` with the new `example.docker-compose.yml` file

4. It then uses `docker compose pull` and `docker compose up -d` to download and rebuild the containers with minimal disruption

### Logging

The script logs all of its actions to a log file in the `logs` directory. This is useful for debugging and monitoring purposes. Each time the script is run this generates a new log file named `ara-update-log_<date>_<time>`.

## Before You Begin

### Make Files Executable (If Not Already)

Before the script is run you must ensure that the files are executable. This can be done by running the following command (assuming from the `Ara` root directory):

```bash
find cli/ -type f -name "*.sh" -exec chmod +x {} \;
```

Because this bash script is modularised it is important that the files are executable so that they can be run. If you do not do this then the script will not run.

### Script Pre-Requisites

This script assumes that you have already set up the an ARA instance and have a running instance and that you have the following:

- An `Ara` instance already setup with a `docker-compose` file and `.env` file (does not have to be running)
- `ARA_HOME` directory defined in `.bashrc`
- Filled in the specific `Update Script` variables in the `.env` file (see below).

### Environment Variables

In order to run the script you must first set up the following variables in the `.env` file:

#### UPDATE_MODE

This variable is corresponds to the type of `Ara` instance you are running. It can be set to the following:

- `single` - for those using the `single-wp-single-db.yml` compose file
- `multi` - for those using the `multi-wp-single-db.yml` compose file

If the variable is not set the script will not run.

#### UPDATE_COMPOSE_FILE

This is an explicit variable that when set will override the options passed through the script. This is especially useful for running the script remotely through `Ansible`. The variable should be set to on eof the following:

- `1` to enable the overwriting of the `docker-compose` with the stated example `docker-compose` file set above and update the git repository as well as the containers. This is the `default` mode.

- `0` to disable the overwriting of the docker-compose file the updating of the repository. This mode WILL ONLY pull the updated docker images and rebuild the containers. This is useful if you want to automate the update process using ansible but do not want to overwrite the `docker-compose` file.

#### Choosing the Wrong Variable Values (Backup File)

If you happen to chose the wrong variable values the script - like `multi` when a `single` instance is running - the script will rewrite the `docker-compose` file. In case this happens the script creates a backup of the current `docker-compose` file in the root directory named `docker-compose.yml.backup.file` which can be used to restore the original file.

Be sure to run `docker-compose down` before restoring the file and to set the `UPDATE` variables accordingly as above before re-running the script.

## Running the Script / Options

The script can be run by executing the following command:

```bash
./cli/update/update.sh
```

```html
default - runs docker-compare, git pull, docker copy and update with user verification prompt
no-prompt - runs same as default without user prompt (if hash different will prompt to copy regardless)
docker-compare - runs hash comparison script only
git-pull - runs only git pull
docker-update - runs the image update script only

---
Special Modes
---
ansible - to be run by external ansible playbook only - will fail if not run by ansible
help - displays help message
```

## Upcoming Features

- Better Error Handling - The script will handle errors better notify users of errors through email or other means

## Ara Update Ansible Playbook

I have created an `ansible playbook` which runs this update script on remote servers. Look at it [here](https://gitlab.com/conorjwryan/ansible/-/tree/main/update/docker/ara?ref_type=heads)
