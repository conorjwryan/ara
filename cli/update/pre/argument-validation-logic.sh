#!/bin/bash
# shellcheck disable=SC2034
# Argument Validation Logic - Ara Update Bash Script
# Location: cli/update/pre/argument-valiation-logic.sh
# Version 1.1.2
# Author: Conor Ryan
# Date: 2023-11-28
# Description: This script is used to check the existance of the ARA_HOME variable
# shellcheck source=../update
####

####
# Argument Validation
####

docker_compare_active=""
docker_copy_active=""
docker_update_active=""
git_pull_active=""
prompt_verfiy=""

echo -e "Checking for Script Mode"

if [[ "$1" == "default" || -z "$1"  ]]; then
    docker_compare_active=1
    docker_copy_active=1
    docker_update_active=1
    git_pull_active=1
    prompt_verfiy=1

elif [[ "$1" == "no-prompt" || "$1" == "ansible" ]]; then 
    docker_compare_active=1
    docker_copy_active=1
    docker_update_active=1
    git_pull_active=1
    prompt_verfiy=0

elif [[ "$1" == "docker-compare" ]]; then
    docker_compare_active=1
    docker_copy_active=0
    docker_update_active=0
    git_pull_active=0
    prompt_verfiy=0

elif [[ "$1" == "docker-update" ]]; then
    docker_compare_active=0
    docker_copy_active=0
    docker_update_active=1
    git_pull_active=0
    prompt_verfiy=0

elif [[ "$1" == "git-pull" ]]; then
    docker_compare_active=0
    docker_copy_active=0
    docker_update_active=0
    git_pull_active=1
    prompt_verfiy=0

elif [[ "$1" == "help" ]]; then
    echo "Usage: ./update.sh [ARGUMENT]"
    echo "";
    echo "Arguments:"
    echo "default - Runs full script process"
    echo "no-prompt - Runs all update scripts without prompting the user"
    echo "docker-compare - Runs only the docker compare script"
    echo "docker-update - Runs only the docker update script"
    echo "git-pull - Runs only the git pull script"
    echo "";
    echo "Special Modes"
    echo "ansible - Runs the script in ansible mode"
    echo "help - Displays this help message"
    echo "";
    echo "Exiting Script"
    exit 0
    
else
    echo "Invalid Argument Passed"
    echo "Please refer to the documentation for a list of valid arguments"
    echo "Aborting Script"
    exit 1

fi
