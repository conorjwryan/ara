#!/bin/bash
# shellcheck disable=SC1091,SC2154,SC2086
# Ara Home Logic - Ara Update Bash Script
# Location: cli/update/pre/ara-home-logic.sh
# Version 1.2.0
# Author: Conor Ryan
# Date: 2024-02-02
# Description: This script is used to check the existance of the ARA_HOME variable
####

log "Checking for existance of ARA Directory"

if [ -z "$ARA_HOME" ]; then
    ARA_HOME="$HOME/ara/"

    echo "";
	log "INFO: ARA_HOME variable is not set. Setting to Default: $ARA_HOME";
    echo "";
      
fi

####
# Check Existance of ARA_HOME Directory
####

if [[ ! -d "$ARA_HOME" ]]; then
	log "Error: ARA_HOME does not exist.\nIf you have Project Ara in another directory please refer to the documentation on how to set the 'ARA_HOME' variable.\nAborting script.\n";
	exit 1;

fi

####
# Check Existance of .env file
####

if [[ ! -f "${ARA_HOME}.env" ]]; then
    log '.env does not exist. Aborting script.'
    exit 1;

fi
