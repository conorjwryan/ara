#! /bin/bash
# Script Prerequisite Logic - Ara Update Bash Script
# Location: cli/update/pre/script-prerequisite-logic.sh
# Version 2.0.0
# Author: Conor Ryan
# Date: 2024-02-02
# Description: This script is used to check the prerequisites are met before running the update script
####

compose_file=""
docker_file="${ARA_HOME}docker-compose.yml"

####
# Check Existance of docker-compose.yml file
####

if [[ ! -f "${docker_file}" ]]; then
    log "ERROR: docker-compose.yml does not exist."
    log "ERROR: Please follow the steps to setup Project Ara before trying to update the repository and docker-compose files. Aborting script."
    exit 1;

fi

if [[ "${UPDATE_MODE}" != "multi" && "${UPDATE_MODE}" != "single" ]]; then
    log "ERROR: Update mode has not been set so the script cannot continue."
    log "ERROR: Please set the UPDATE_MODE environment variable to either 'multi' or 'single'. Aborting script."
    exit 1;

fi

if [[ "${UPDATE_MODE}" == "multi" ]]; then
    compose_file="${ARA_HOME}docker/compose/multi-wp-single-db.yml"

elif [[ "${UPDATE_MODE}" == "single" ]]; then
    compose_file="${ARA_HOME}docker/compose/single-wp-single-db.yml"

fi

if [[ ! -f "${compose_file}" ]]; then
    log "ERROR: ${compose_file} does not exist."
    log "ERROR: Please follow the steps to setup Project Ara before trying to update the repository and docker-compose files. Aborting script."
    exit 1;

fi

if [[ ${UPDATE_COMPOSE_FILE} == "" ]]; then
    log "ERROR: UPDATE_COMPOSE environment variable has not been set so the script cannot continue."
    log "ERROR: Please set the UPDATE_COMPOSE environment variable to either '1' or '0'. Aborting script."
    exit 1;

fi

