#!/bin/bash
# shellcheck source=/dev/null
# shellcheck disable=SC2154
# Update Log Process Function - Ara Update Bash Script
# Location: cli/update/functions/log-process-function.sh
# Version 1.0.2
# Author: Conor Ryan
# Date: 2023-11-28
# Description: This script includes the log setup and functions needed for the update script
####

if [ ! -d "${script_dir}logs" ]; then
    mkdir "${script_dir}logs"
    if [ ! -d "${script_dir}logs" ]; then
        echo -e "Error: Unable to create logs directory"
        echo -e "Check permissions and try again"
        exit 1
    fi
fi

log_time=$(date '+%Y-%m-%d_%H-%M-%S')

if [ ! -f "${script_dir}logs/ara-update-log_${log_time}.log" ]; then
        touch "${script_dir}logs/ara-update-log_${log_time}.log"
        if [ ! -f "${script_dir}logs/ara-update-log_${log_time}.log" ]; then
            echo -e "Error: Unable to create log file\n"
            echo -e "Check permissions and try again"
            exit 1
        fi
fi

log() {

    log_file="${script_dir}logs/ara-update-log_${log_time}.log"

    echo -e "$1";
    echo "$(date '+%Y-%m-%d %H:%M:%S') - $1" >> "${log_file}"

}

log_break () {
        echo -e "####################" >> "${log_file}"

}