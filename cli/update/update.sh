#!/bin/bash
# shellcheck disable=SC1091,SC2154,SC2086,SC2034
# Main Script - Ara Update Bash Script
# Location: cli/update/update.sh
# Version 1.3.5
# Author: Conor Ryan
# Date: 2024-02-02
# Description: This script is used to update Project Ara
# shellcheck source=pre/ara-home-logic.sh
####

####
# Ansible Specific Logic
####
# Need to set ARA_HOME before changing to script directory
####

if [[ $1 == "ansible" ]]; then
    ARA_HOME="$(pwd)/"

fi

####
# Set Script Directory Variable
####

cd "$(dirname "$0")" || exit 1;

script_dir="$(pwd)/";

####
# Checks if the arguments passed to the script are valid
####

source ./pre/argument-validation-logic.sh

####
# Log Setup
####

source ./functions/log-process-function.sh

if [[ -z $1 ]]; then
    log "Script Mode: 'default'"

else
    log "Script Mode: '$1'"

fi

####
# Include Ara Home Logic
####

source ./pre/ara-home-logic.sh

####
# Setting Variables
####

set -o allexport
source "${ARA_HOME}".env 
set +o allexport

####
# Checking Prerequisites
####

source ./pre/script-prerequisite-logic.sh

####
# Compare Docker Compose Files Through Hashes
####

if [[ ${UPDATE_COMPOSE_FILE} == 1 ]]; then


    if [[ ${docker_compare_active} == 1 ]]; then
        source ./main/docker-compare-logic.sh

    fi

    ####
    # Pulling Latest Version of Project Ara
    ####

    if [[ ${git_pull_active} == 1 ]]; then
        source ./main/git-pull-logic.sh

    fi

    ####
    # Copy Docker Compose Files
    ####

    if [[ ${docker_copy_active} == 1 ]]; then
        source ./main/docker-copy-logic.sh

    fi

else 
    log "INFO: UPDATE_COMPOSE is set to 0";
    log "INFO: Ara Repository Not Updated";
    log "INFO: Docker Compose File not Overwritten";
    log "INFO: Only Docker Containers will be updated";
    log "INFO: This setting cannot be changed by the script for safety reasons";
    log "INFO: Manually change the UPDATE_COMPOSE environment variable to '1' to update the repository and docker-compose files using this script";
    echo "";
fi

####
# Update Docker Compose Files
####

if [[ ${docker_update_active} == 1 ]]; then
    source ./main/docker-update-logic.sh

fi

####
# Set Validation back to Default
####

docker_compare_active=""
docker_copy_active=""
docker_update_active=""
git_pull_active=""
prompt_verfiy=""

log "Script Complete";
log_break

exit 0;