#! /bin/bash
# shellcheck disable=SC2034,SC2086,SC2154,SC2181
# Docker Compare Logic - Ara Update Bash Script
# Location: cli/update/main/docker-compare-logic.sh
# Version 1.2.0
# Author: Conor Ryan
# Date: 2024-02-02
# Description: This script is used to compare docker-compose files with Hashes
# shellcheck source=docker-update-logic.sh
####

hash_compare=""

log "Comparing Docker Compose Files";

hash_inuse=$(md5sum ${docker_file} | awk '{print $1}')
hash_example=$(md5sum ${compose_file} | awk '{print $1}')

# Compare the hashes of the files
if [ "${hash_inuse}" == "${hash_example}" ]; then
    # If the hashes are the same
    hash_compare="same"

    echo "";
    log "INFO: The files have the same hashes";
    log "The script will rerwrite the docker-compose.yml file"
    
    if [[ ${prompt_verfiy} == 1 ]]; then
        # Prompt user to continue or abort
        while true; do
            read -r -p "Do you wish to continue? (y/n)" yn
            case $yn in
                [Yy]* ) log  "Continuing"; break;;
                [Nn]* ) log "Aborting"; log "Chose not to continue"; log_break; exit 0;;
                * ) log "Please answer yes (y) or no (n).";;
            esac
        done;
    fi

else
    # If the hashes are different
    log "The files have different hashes meaning the docker-compose.yml file has been changed"
    log "Here is a summary of the changes:";
    echo "";

    # Show the changes
    diff ${docker_file} ${compose_file}

    # Checking if Ansible is running and skipping the prompt if it is
    if [[ "$1" == "ansible" ]]; then

    hash_compare="different-allowed";
    log "INFO: Skipping User Prompt as Ansible is running";

    else
        # Prompt user to continue or abort
        while true; do
            read -r -p "Do you wish to continue? A backup of the current docker-compose file will be made. (y/n)" yn
            case $yn in
                [Yy]* ) 
                log "Continuing";
                echo "";
                hash_compare="different-allowed";
                break;
                ;;

                [Nn]* ) 
                log "Aborting";
                log_break;
                exit 0;;
                * ) log "Please answer yes (y) or no (n).";
                echo "";
                ;;
            esac
        done;
        # end of prompt 

    fi
    # end of ansible check

fi
# end of hash compare check