#! /bin/bash
# shellcheck disable=SC1091,SC2154,SC2086,SC2181
# Docker Copy Logic - Ara Update Bash Script
# Location: cli/update/main/docker-copy-logic.sh
# Version 1.3.0
# Author: Conor Ryan
# Date: 2024-02-02
# Description: This script is used to copy docker-compose files
####

####
# Copy docker-compose files
####
# For security reasons this script will only run if the docker-compare-logic.sh is run first.
# This is to ensure that the docker-compose file is not overwritten by accident.
####

if [[ ${hash_compare} == "same" || ${hash_compare} == "different-allowed" ]]; then
    log "The script will save the current docker-compose.yml file as docker-compose.yml.backup.file in ${ARA_HOME}"

    # Save the current docker-compose.yml file as docker-compose.yml.backup.file
    cp ${docker_file} ${docker_file}.backup.file || exit 1
    if [[ $? -eq 0 ]]; then
        log "Backup of 'docker-compose.yml' file made";

    else
        echo "";
        log "Error: Failed to make backup of docker-compose.yml file."
        log "Make sure you have the right permissions.";
        exit 1;

    fi

    log "Updating Docker Compose File From Example";
    
    cp ${compose_file} ${docker_file} || exit 1
    if [[ $? -eq 0 ]]; then
        log "Example Docker Compose Copied Over Successfully";

    else
        log "Failed to Copy over Docker Compose File.";
        log "Make sure you have the right permissions.";
        exit 1;

    fi

else 
    log "INFO: Docker Compose File Not Updated";

fi
