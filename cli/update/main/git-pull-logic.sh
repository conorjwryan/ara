#! /bin/bash
# shellcheck disable=SC2034,SC2154
# Git Pull Logic - Ara Update Bash Script
# Location: cli/update/main/git-pull-logic.sh
# Version 1.0.4
# Author: Conor Ryan
# Date: 2023-02-02
# Description: This script is used to pull the latest version of Project Ara
####

echo "";
log "Pulling Latest Version of Project Ara";
echo "";

cd "${ARA_HOME}" || exit

# Check if there are any changes before the pull
if [[ $(git status --porcelain) ]]; then
    log "Error: Changes detected before pull";
    log "Please commit or stash your changes before pulling the latest version";
    exit 1;

fi

if git pull; then
    # Check if there are any changes after the pull
    if [[ $(git status --porcelain) ]]; then
        echo "";
        log "Latest version pulled with changes";
        echo "";
        git_logic="updated"

    else
        echo "";
        log "Latest version already pulled";
        echo "";
        git_logic="no_changes"

    fi
    # continue with the script
else
    echo "";
    log "Error: Failed to pull latest version";
    exit 1;
    
fi


cd "${script_dir}" || exit 1;