#! /bin/bash
# shellcheck disable=SC1091,SC2154,SC2086
# Docker Update Logic - Ara Update Bash Script
# Location: cli/update/main/docker-update-logic.sh
# Version 1.0.6
# Author: Conor Ryan
# Date: 2024-02-02
# Description: This script is used to update docker-compose files
####

if [[ ${prompt_verfiy} == 1 ]]; then
	while true; do
		read -r -p "Would you like to update the images in the docker-compose file? (y/n):" yn
			case $yn in
			
				[Yy]* ) log "Chosen YES";
						echo "";
						break;
						;;
				[Nn]* ) log "Chosen NO, the script will now exit";
						log_break;
						exit 0;
						;;
				* ) log "Please answer yes (y) or no (n).";
					;;
			esac
	done

fi

log "Updating Docker Images";
echo "";

cd "${ARA_HOME}" || exit 1;

docker compose pull || exit 1;

echo "";
log "Docker Compose Images Downloaded";
log "Rebuilding";
echo "";

docker compose up -d --force-recreate  || exit 1;

echo "";
log "Docker Compose Images Updated";

cd "${script_dir}" || exit 1;