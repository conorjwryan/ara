# CLI Folder Information

---

Location: ara/cli

---

The cli folder contains the command line scripts for the project. The scripts are written in bash and are used to automate the setup, deployment and maintenance of the sites in this project.

## Scripts

### Update Script

This script is used to update the project to the latest version. It pulls the latest version from the git repository and then updates the docker images and containers.

### Backup Script

This script is used to backup the database and files for the project. It backs up, encrypts and transfers the files to specified remote servers. The script can be run daily through a cron job or manually. To explore the various options for the script go to the Backup Script README.
